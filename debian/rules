#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/architecture.mk
ifneq ($(DEB_BUILD_ARCH),$(DEB_HOST_ARCH))
CROSS = CROSS=$(DEB_HOST_GNU_TYPE)- PKG_CONFIG=$(DEB_HOST_GNU_TYPE)-pkg-config
endif

# The following architectures need the -latomic flag
# to build, otherwise we FTBFS with
#    ./track_info.c:47: undefined reference to `__atomic_fetch_add_8'
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=935678
# Depends on 13-atomic_ld.patch
ifneq (,$(findstring $(DEB_BUILD_ARCH), armel m68k mipsel powerpc sh4))
export LDLIBS += -latomic
endif

DEB_LDFLAGS_MAINT_APPEND += -Wl,--as-needed
DEB_CFLAGS_MAINT_APPEND  += -I/usr/include/ncursesw
DEB_BUILD_MAINT_OPTIONS   = hardening=+all
DPKG_EXPORT_BUILDFLAGS    = 1
include /usr/share/dpkg/buildflags.mk

suggested_deps = pulse roar jack

EXTRA_CMUS_DIR_OP_PLUGINS = debian/cmus/usr/lib/cmus/op/
EXTRA_CMUS_PLUGINS := $(foreach plugin,$(suggested_deps),$(plugin).so)

%:
	dh $@ --with bash-completion

override_dh_auto_configure:
	$(CROSS) ./configure \
		prefix=/usr \
		CONFIG_ARTS=n \
		DEBUG=0

override_dh_auto_build:
	# Pass V=2 to make to enable verbose build logs, which is useful for
	# porters, sorting out build hardening issues, etc.
	dh_auto_build -- V=2

override_dh_install:
	dh_install -pcmus
	dh_movefiles -pcmus-plugin-ffmpeg --sourcedir=debian/cmus/ \
		/usr/lib/cmus/ip/ffmpeg.so

override_dh_installdocs:
	dh_installdocs
	# do not install zsh and bash completion twice
	rm debian/cmus/usr/share/doc/cmus/contrib/_cmus \
		debian/cmus/usr/share/doc/cmus/contrib/cmus.bash-completion

override_dh_shlibdeps:
	dh_shlibdeps -pcmus $(foreach plugin,$(EXTRA_CMUS_PLUGINS),-X$(plugin))
	dpkg-shlibdeps -O -dSuggests \
		$(foreach plugin,$(EXTRA_CMUS_PLUGINS),$(EXTRA_CMUS_DIR_OP_PLUGINS)$(plugin)) \
		>> debian/cmus.substvars
	dh_shlibdeps --remaining-packages
